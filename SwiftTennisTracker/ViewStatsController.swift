//
//  ViewStatsController.swift
//  SwiftTennisTracker
//
//  Created by Bryan Winmill on 11/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class ViewStatsController: UIViewController {
    
    //Object of Player and Opponent Stats
    var stat:PFObject!
    
    //Player Labels
    @IBOutlet var playerFirstServePercent: UILabel!
    @IBOutlet var playerFirstServeWins: UILabel!
    @IBOutlet var playerFirstServeAces: UILabel!
    @IBOutlet var playerFirstServeWinners: UILabel!
    @IBOutlet var playerSecondServePercent: UILabel!
    @IBOutlet var playerSecondServeWins: UILabel!
    @IBOutlet var playerSecondServeAces: UILabel!
    @IBOutlet var playerSecondServeWinners: UILabel!
    @IBOutlet var playerDoubleFaults: UILabel!
    @IBOutlet var playerBehindBaseline: UILabel!
    @IBOutlet var playerBehindBaselineWins: UILabel!
    @IBOutlet var playerBehindBaselineLosses: UILabel!
    @IBOutlet var playerOnBaseline: UILabel!
    @IBOutlet var playerOnBaselineWins: UILabel!
    @IBOutlet var playerOnBaselineLosses: UILabel!
    @IBOutlet var playerBeforeBaseline: UILabel!
    @IBOutlet var playerBeforeBaselineWins: UILabel!
    @IBOutlet var playerBeforeBaselineLosses: UILabel!
    @IBOutlet var playerUnsuccessfulReturns: UILabel!
    @IBOutlet var playerWinners: UILabel!
    @IBOutlet var playerForcedErrors: UILabel!
    @IBOutlet var playerUnforcedErrors: UILabel!
    
    //Opponent Labels
    @IBOutlet var opponentFirstServePercent: UILabel!
    @IBOutlet var opponentFirstServeWins: UILabel!
    @IBOutlet var opponentFirstServeAces: UILabel!
    @IBOutlet var opponentFirstServeWinners: UILabel!
    @IBOutlet var opponentSecondServePercent: UILabel!
    @IBOutlet var opponentSecondServeWins: UILabel!
    @IBOutlet var opponentSecondServeAces: UILabel!
    @IBOutlet var opponentSecondServeWinners: UILabel!
    @IBOutlet var opponentDoubleFaults: UILabel!
    @IBOutlet var opponentBehindBaseline: UILabel!
    @IBOutlet var opponentBehindBaselineWins: UILabel!
    @IBOutlet var opponentBehindBaselineLosses: UILabel!
    @IBOutlet var opponentOnBaseline: UILabel!
    @IBOutlet var opponentOnBaselineWins: UILabel!
    @IBOutlet var opponentOnBaselineLosses: UILabel!
    @IBOutlet var opponentBeforeBaseline: UILabel!
    @IBOutlet var opponentBeforeBaselineWins: UILabel!
    @IBOutlet var opponentBeforeBaselineLosses: UILabel!
    @IBOutlet var opponentUnsuccessfulReturns: UILabel!
    @IBOutlet var opponentWinners: UILabel!
    @IBOutlet var opponentForcedErrors: UILabel!
    @IBOutlet var opponentUnforcedErrors: UILabel!
    
    func loadStats() {
        
        var temp : Double = 0;
        
        //////////////////////////////
        /////////  Player   //////////
        //////////////////////////////
        
        temp = (stat.object(forKey: "playerTotalFirstServes") as! Double )
        if ( temp != -1 ) {
            
            temp = 0
            temp = (stat.object(forKey: "playerTotalFirstServes") as! Double )
            
            if ( temp != 0 ) {
                playerFirstServePercent.text = "1st Serves In: " + String(describing: (stat.object(forKey: "playerFirstServePercent") ?? 0)) + "%"
                playerFirstServeWins.text = "1st Serve Wins: " + String(describing: (stat.object(forKey: "playerFirstServePointWins") ?? 0)) + "%"
                playerFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerFirstServeAces") ?? 0))
                playerFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerFirstServeWinners") ?? 0))
            } else {
                playerFirstServePercent.text = "1st Serves In: NA"
                playerFirstServeWins.text = "1st Serve Wins: NA"
                playerFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerFirstServeAces") ?? 0))
                playerFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerFirstServeWinners") ?? 0))
            }
            
            
            temp = 0
            temp = (stat.object(forKey: "playerTotalSecondServes") as! Double )
            
            if ( temp != 0 ) {
                
                playerSecondServePercent.text = "2nd Serves In: " + String(describing: (stat.object(forKey: "playerSecondServePercent") ?? 0)) + "%"
                playerSecondServeWins.text = "2nd Serve Wins: " + String(describing: (stat.object(forKey: "playerSecondServePointWins") ?? 0)) + "%"
                playerSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerSecondServeAces") ?? 0))
                playerSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerSecondServeWinners") ?? 0))
            } else {
                playerSecondServePercent.text = "2nd Serves In: NA"
                playerSecondServeWins.text = "2nd Serve Wins: NA"
                playerSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerSecondServeAces") ?? 0))
                playerSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerSecondServeWinners") ?? 0))
            }
            
            playerDoubleFaults.text = "Double Faults: " + String(describing: (stat.object(forKey: "playerDoubleFaults") ?? 0))
            
        } else {
            playerFirstServePercent.text = "1st Serves In: NA"
            playerFirstServeWins.text = "1st Serve Wins: NA"
            playerFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerFirstServeAces") ?? 0))
            playerFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerFirstServeWinners") ?? 0))
            playerSecondServePercent.text = "2nd Serves In: NA"
            playerSecondServeWins.text = "2nd Serve Wins: NA"
            playerSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "playerSecondServeAces") ?? 0))
            playerSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerSecondServeWinners") ?? 0))
            playerDoubleFaults.text = "Double Faults: " + String(describing: (stat.object(forKey: "playerDoubleFaults") ?? 0))
        }
        
        temp = 0
        temp = (stat.object(forKey: "playerReturnsBehindBaseline") as! Double )
        
        if ( temp != -1 ) {
            
            temp = 0
            temp = (stat.object(forKey: "playerReturnsBehindBaseline") as! Double )
            
            if (temp != 0) {
                
                playerBehindBaseline.text = "Behind Baseline: " + String(describing: (stat.object(forKey: "playerReturnsBehindBaseline") ?? 0)) + "%"
                playerBehindBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "playerReturnsBehindBaselineWins") ?? 0)) + "%"
                playerBehindBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "playerReturnsBehindBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                playerBehindBaseline.text = "Behind Baseline: " + String(describing: (stat.object(forKey: "playerReturnsBehindBaseline") ?? 0)) + "%"
                playerBehindBaselineWins.text = "Wins: NA"
                playerBehindBaselineLosses.text = "Losses: NA"
                
            }
            
            temp = 0
            temp = (stat.object(forKey: "playerReturnsOnBaseline") as! Double )
            
            if (temp != 0) {
                
                playerOnBaseline.text = "On Baseline: " + String(describing: (stat.object(forKey: "playerReturnsOnBaseline") ?? 0)) + "%"
                playerOnBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "playerReturnsOnBaselineWins") ?? 0)) + "%"
                playerOnBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "playerReturnsOnBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                playerOnBaseline.text = "On Baseline: " + String(describing: (stat.object(forKey: "playerReturnsOnBaseline") ?? 0)) + "%"
                playerOnBaselineWins.text = "Wins: NA"
                playerOnBaselineLosses.text = "Losses: NA"
                
            }
            
            temp = 0
            temp = (stat.object(forKey: "playerReturnsInFrontOfBaseline") as! Double )
            
            if (temp != 0) {
                
                playerBeforeBaseline.text = "In Front Of Baseline: " + String(describing: (stat.object(forKey: "playerReturnsInFrontOfBaseline") ?? 0)) + "%"
                playerBeforeBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "playerReturnsInFrontOfBaselineWins") ?? 0)) + "%"
                playerBeforeBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "playerReturnsInFrontOfBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                playerBeforeBaseline.text = "In Front Of Baseline: " + String(describing: (stat.object(forKey: "playerReturnsInFrontOfBaseline") ?? 0)) + "%"
                playerBeforeBaselineWins.text = "Wins: NA"
                playerBeforeBaselineLosses.text = "Losses: NA"
                
            }
            
        } else {
            
            playerBehindBaseline.text = "Behind Baseline: NA"
            playerBehindBaselineWins.text = "Wins: NA"
            playerBehindBaselineLosses.text = "Losses: NA"
            playerOnBaseline.text = "On Baseline: NA"
            playerOnBaselineWins.text = "Wins: NA"
            playerOnBaselineLosses.text = "Losses: NA"
            playerBeforeBaseline.text = "In Front Of Baseline: NA"
            playerBeforeBaselineWins.text = "Wins: NA"
            playerBeforeBaselineLosses.text = "Losses: NA"
            
        }
        
        playerUnsuccessfulReturns.text = "Unsuccessful Returns: " + String(describing: (stat.object(forKey: "playerTotalUnsuccessfulReturns") ?? 0))
        playerWinners.text = "Winners: " + String(describing: (stat.object(forKey: "playerTotalWinners") ?? 0))
        playerForcedErrors.text = "Forced Errors: " + String(describing: (stat.object(forKey: "playerForcedErrors") ?? 0))
        playerUnforcedErrors.text = "Unforced Errors: " + String(describing: (stat.object(forKey: "playerUnforcedErrors") ?? 0))
        
        
        ////////////////////////////////
        /////////  Opponent   //////////
        ////////////////////////////////
        
        temp = 0
        temp = (stat.object(forKey: "opponentTotalFirstServes") as! Double )
        
        if ( temp != -1 ) {
            
            temp = 0
            temp = (stat.object(forKey: "opponentTotalFirstServes") as! Double )
            
            if ( temp != 0 ) {
                opponentFirstServePercent.text = "1st Serves In: " + String(describing: (stat.object(forKey: "opponentFirstServePercent") ?? 0)) + "%"
                opponentFirstServeWins.text = "1st Serve Wins: " + String(describing: (stat.object(forKey: "opponentFirstServePointWins") ?? 0)) + "%"
                opponentFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentFirstServeAces") ?? 0))
                opponentFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentFirstServeWinners") ?? 0))
            } else {
                opponentFirstServePercent.text = "1st Serves In: NA"
                opponentFirstServeWins.text = "1st Serve Wins: NA"
                opponentFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentFirstServeAces") ?? 0))
                opponentFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentFirstServeWinners") ?? 0))
            }
            
            
            temp = 0
            temp = (stat.object(forKey: "opponentTotalSecondServes") as! Double )
            
            if ( temp != 0 ) {
                
                opponentSecondServePercent.text = "2nd Serves In: " + String(describing: (stat.object(forKey: "opponentSecondServePercent") ?? 0)) + "%"
                opponentSecondServeWins.text = "2nd Serve Wins: " + String(describing: (stat.object(forKey: "opponentSecondServePointWins") ?? 0)) + "%"
                opponentSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentSecondServeAces") ?? 0))
                opponentSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentSecondServeWinners") ?? 0))
            } else {
                
                opponentSecondServePercent.text = "2nd Serves In: NA"
                opponentSecondServeWins.text = "2nd Serve Wins: NA"
                opponentSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentSecondServeAces") ?? 0))
                opponentSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentSecondServeWinners") ?? 0))
            }
            
            opponentDoubleFaults.text = "Double Faults: " + String(describing: (stat.object(forKey: "opponentDoubleFaults") ?? 0))
            
        } else {
            opponentFirstServePercent.text = "1st Serves In: NA"
            opponentFirstServeWins.text = "1st Serve Wins: NA"
            opponentFirstServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentFirstServeAces") ?? 0))
            opponentFirstServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentFirstServeWinners") ?? 0))
            opponentSecondServePercent.text = "2nd Serves In: NA"
            opponentSecondServeWins.text = "2nd Serve Wins: NA"
            opponentSecondServeAces.text = "Aces: " + String(describing: (stat.object(forKey: "opponentSecondServeAces") ?? 0))
            opponentSecondServeWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentSecondServeWinners") ?? 0))
            opponentDoubleFaults.text = "Double Faults: " + String(describing: (stat.object(forKey: "opponentDoubleFaults") ?? 0))
        }
        
        temp = 0
        temp = (stat.object(forKey: "opponentReturnsBehindBaseline") as! Double )
        
        if ( temp != -1 ) {
            
            temp = 0
            temp = (stat.object(forKey: "opponentReturnsBehindBaseline") as! Double )
            
            if (temp != 0) {
                
                opponentBehindBaseline.text = "Behind Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsBehindBaseline") ?? 0)) + "%"
                opponentBehindBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "opponentReturnsBehindBaselineWins") ?? 0)) + "%"
                opponentBehindBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "opponentReturnsBehindBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                opponentBehindBaseline.text = "Behind Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsBehindBaseline") ?? 0)) + "%"
                opponentBehindBaselineWins.text = "Wins: NA"
                opponentBehindBaselineLosses.text = "Losses: NA"
                
            }
            
            temp = 0
            temp = (stat.object(forKey: "opponentReturnsOnBaseline") as! Double )
            
            if (temp != 0) {
                
                opponentOnBaseline.text = "On Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsOnBaseline") ?? 0)) + "%"
                opponentOnBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "opponentReturnsOnBaselineWins") ?? 0)) + "%"
                opponentOnBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "opponentReturnsOnBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                opponentOnBaseline.text = "On Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsOnBaseline") ?? 0)) + "%"
                opponentOnBaselineWins.text = "Wins: NA"
                opponentOnBaselineLosses.text = "Losses: NA"
                
            }
            
            temp = 0
            temp = (stat.object(forKey: "opponentReturnsInFrontOfBaseline") as! Double )
            
            if (temp != 0) {
                
                opponentBeforeBaseline.text = "In Front Of Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsInFrontOfBaseline") ?? 0)) + "%"
                opponentBeforeBaselineWins.text = "Wins: " + String(describing: (stat.object(forKey: "opponentReturnsInFrontOfBaselineWins") ?? 0)) + "%"
                opponentBeforeBaselineLosses.text = "Losses: " + String(describing: (stat.object(forKey: "opponentReturnsInFrontOfBaselineLosses") ?? 0)) + "%"
                
            } else {
                
                opponentBeforeBaseline.text = "In Front Of Baseline: " + String(describing: (stat.object(forKey: "opponentReturnsInFrontOfBaseline") ?? 0)) + "%"
                opponentBeforeBaselineWins.text = "Wins: NA"
                opponentBeforeBaselineLosses.text = "Losses: NA"
                
            }
            
        } else {
            
            opponentBehindBaseline.text = "Behind Baseline: NA"
            opponentBehindBaselineWins.text = "Wins: NA"
            opponentBehindBaselineLosses.text = "Losses: NA"
            opponentOnBaseline.text = "On Baseline: NA"
            opponentOnBaselineWins.text = "Wins: NA"
            opponentOnBaselineLosses.text = "Losses: NA"
            opponentBeforeBaseline.text = "In Front Of Baseline: NA"
            opponentBeforeBaselineWins.text = "Wins: NA"
            opponentBeforeBaselineLosses.text = "Losses: NA"
            
        }
        
        opponentUnsuccessfulReturns.text = "Unsuccessful Returns: " + String(describing: (stat.object(forKey: "opponentTotalUnsuccessfulReturns") ?? 0))
        opponentWinners.text = "Winners: " + String(describing: (stat.object(forKey: "opponentTotalWinners") ?? 0))
        opponentForcedErrors.text = "Forced Errors: " + String(describing: (stat.object(forKey: "opponentForcedErrors") ?? 0))
        opponentUnforcedErrors.text = "Unforced Errors: " + String(describing: (stat.object(forKey: "opponentUnforcedErrors") ?? 0))
        
    }
    
    @IBAction func deleteContact(_ sender: UIBarButtonItem) {
        
        stat.deleteInBackground()
        
        self.performSegue(withIdentifier: "toStats", sender: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadStats()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
