//
//  ViewController.swift
//  SwiftTennisTracker
//
//  Created by Bryan Winmill on 11/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var stats: [PFObject] = []
    
    @IBOutlet var tableView: UITableView!
    

    @IBAction func signOutButton(_ sender: UIBarButtonItem) {
        
        PFUser.logOutInBackground { (error) in
            print(PFUser.current()?["email"] as? String )
        }
        
        self.performSegue(withIdentifier: "signOut", sender: nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        loadStatsArray {
            self.update()
        }
    }
    
    func update() {
        tableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        update()
    }
    
    func loadStatsArray(completion: @escaping () -> Void) {
        
        let query = PFQuery(className:"Match")
        query.whereKey("email", equalTo: PFUser.current()?["email"] as? String)
        query.findObjectsInBackground { (objects, error) in
            if error == nil {
                //Success
                
                if let objects = objects {
                    for object in objects {
                        self.stats.append(object)
                    }
                    completion()
                }
            } else {
                print("Error")
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        return stats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let stat = stats[indexPath.row]
        cell.textLabel?.text = stat.object(forKey: "title") as! String
        
        return cell
    }
    
    var selectedIndex = 0
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        self.performSegue(withIdentifier: "loadStats", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "loadStats"{
            if let vc = segue.destination as? ViewStatsController {
                vc.stat = stats[selectedIndex]
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

