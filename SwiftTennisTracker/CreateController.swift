//
//  CreateController.swift
//  SwiftTennisTracker
//
//  Created by Bryan Winmill on 11/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class CreateController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var firstName: UITextField!
    @IBOutlet var lastName: UITextField!
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var passwordVerify: UITextField!
    
    @IBAction func createButton(_ sender: UIButton) {
        
        if (firstName.text == "" || lastName.text == "" || email.text == "" || password.text == "" || passwordVerify.text == "") {
            
            var alert = UIAlertController(title:"Alert", message:"Some fields are blank!", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated:true, completion:nil)
            
        } else {
        
            if (password.text != passwordVerify.text) {
                
                var alert = UIAlertController(title:"Alert", message:"Your passwords do not match!", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated:true, completion:nil)
                
                password.text = ""
                passwordVerify.text = ""
                
            } else {
            
                let user = PFUser()
            
                user.username = email.text
                user.password = password.text
                user.email = email.text
            
                user["firstName"] = firstName.text
                user["lastName"] = lastName.text
            
                user.signUpInBackground { (s, e) in
                    if let e = e {
                        _ = e._userInfo! as? NSString
                    } else {
                        
                        [PFUser.logInWithUsername(inBackground: self.email.text!, password: self.password.text!, block: { (user, error) in
                            if(user != nil) {
                                
                            }
                        })]
                        
                        self.performSegue(withIdentifier: "create", sender: nil)
                    }
                }
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.firstName.delegate = self
        self.lastName.delegate = self
        self.email.delegate = self
        self.password.delegate = self
        self.passwordVerify.delegate = self
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
