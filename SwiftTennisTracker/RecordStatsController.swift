//
//  RecordStatsController.swift
//  SwiftTennisTracker
//
//  Created by Bryan Winmill on 11/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class RecordStatsController: UIViewController, UITextFieldDelegate {
    
    //Radio Button clicked/unclicked
    var playerServing = false
    var opponentServing = false
    
    var firstServeIn = false
    var secondServeIn = false
    var doubleFault = false
    
    var serveAce = false
    var serveWinner = false
    var serveReturned = false
    var doubleFaultOne = false
    
    var behindBaseline = false
    var onBaseline = false
    var inFrontOfBaseline = false
    var returnWinner = false
    var unsuccessfulReturn = false
    var serverDF = false
    
    var playerWon = false
    var opponentWon = false
    
    var winByAce = false
    var winByWinner = false
    var winByForced = false
    var winByUnforced = false
    
    //Totals number of points recorded
    var numberOfPointsRecorded = 0
    
    //Player serve statistics
    var playerFirstServePercent: Double = 0.0
    var playerFirstServeSuccessful: Double = 0
    var playerFirstServePointWins: Double = 0
    var playerFirstServePointLosses: Double = 0
    var playerFirstServeAces: Double = 0
    var playerFirstServeWinners: Double = 0
    var playerSecondServePercent: Double = 0
    var playerSecondServeSuccessful: Double = 0
    var playerSecondServePointWins: Double = 0
    var playerSecondServePointLosses: Double = 0
    var playerSecondServeAces: Double = 0
    var playerSecondServeWinners: Double = 0
    var playerDoubleFaults: Double = 0
    var playerTotalFirstServes: Double = 0
    var playerTotalSecondServes: Double = 0
    
    //Player return statistics
    var playerReturnsBehindBaseline: Double = 0
    var playerReturnsBehindBaselineWins: Double = 0
    var playerReturnsBehindBaselineLosses: Double = 0
    var playerReturnsOnBaseline: Double = 0
    var playerReturnsOnBaselineWins: Double = 0
    var playerReturnsOnBaselineLosses: Double = 0
    var playerReturnsInFrontOfBaseline: Double = 0
    var playerReturnsInFrontOfBaselineWins: Double = 0
    var playerReturnsInFrontOfBaselineLosses: Double = 0
    var playerTotalSuccessfulReturns: Double = 0
    var playerTotalUnsuccessfulReturns: Double = 0
    
    //Player point results
    var playerTotalServeAces: Double = 0
    var playerTotalWinners: Double = 0
    var playerForcedErrors: Double = 0
    var playerUnforcedErrors: Double = 0
    
    //Opponent serve statistics
    var opponentFirstServePercent: Double = 0
    var opponentFirstServeSuccessful: Double = 0
    var opponentFirstServePointWins: Double = 0
    var opponentFirstServePointLosses: Double = 0
    var opponentFirstServeAces: Double = 0
    var opponentFirstServeWinners: Double = 0
    var opponentSecondServePercent: Double = 0
    var opponentSecondServeSuccessful: Double = 0
    var opponentSecondServePointWins: Double = 0
    var opponentSecondServePointLosses: Double = 0
    var opponentSecondServeAces: Double = 0
    var opponentSecondServeWinners: Double = 0
    var opponentDoubleFaults: Double = 0
    var opponentTotalFirstServes: Double = 0
    var opponentTotalSecondServes: Double = 0
    
    //Opponent return statistics
    var opponentReturnsBehindBaseline: Double = 0
    var opponentReturnsBehindBaselineWins: Double = 0
    var opponentReturnsBehindBaselineLosses: Double = 0
    var opponentReturnsOnBaseline: Double = 0
    var opponentReturnsOnBaselineWins: Double = 0
    var opponentReturnsOnBaselineLosses: Double = 0
    var opponentReturnsInFrontOfBaseline: Double = 0
    var opponentReturnsInFrontOfBaselineWins: Double = 0
    var opponentReturnsInFrontOfBaselineLosses: Double = 0
    var opponentTotalSuccessfulReturns: Double = 0
    var opponentTotalUnsuccessfulReturns: Double = 0
    
    //Opponent point results
    var opponentTotalServeAces: Double = 0
    var opponentTotalWinners: Double = 0
    var opponentForcedErrors: Double = 0
    var opponentUnforcedErrors: Double = 0
    
    var sectionOne = 0
    var sectionTwo = 0
    var sectionThree = 0
    var sectionFour = 0
    var sectionFive = 0
    var sectionSix = 0
    
    
    @IBOutlet var servingButton: DLRadioButton!
    @IBOutlet var succussfulServeButton: DLRadioButton!
    @IBOutlet var serveResultButton: DLRadioButton!
    
    @IBAction func servingButtonAction(_ sender: DLRadioButton) {
        
        playerServing = false
        opponentServing = false
        sectionOne = 1
        
        if sender.tag == 1 {
            print("Player")
            playerServing = true
        } else if sender.tag == 2 {
            print("Opponenet")
            opponentServing = true
        }
    }    
    
    @IBAction func successfulServeButtonAction(_ sender: DLRadioButton) {
        
        firstServeIn = false
        secondServeIn = false
        doubleFault = false
        sectionTwo = 1
        
        if sender.tag == 1 {
            print("First")
            firstServeIn = true
        } else if sender.tag == 2 {
            print("Second")
            secondServeIn = true
        } else if sender.tag == 3 {
            print("Double Fault")
            doubleFault = true
        }
    }
    
    @IBAction func serveResultButtonAction(_ sender: DLRadioButton) {
        
        serveAce = false
        serveWinner = false
        serveReturned = false
        doubleFaultOne = false
        sectionThree = 1
        
        if sender.tag == 1 {
            print("Ace")
            serveAce = true
        } else if sender.tag == 2 {
            print("Winner")
            serveWinner = true
        } else if sender.tag == 3 {
            print("Returned")
            serveReturned = true
        } else if sender.tag == 4 {
            print("DoubleFault")
            doubleFaultOne = true
        }
    }
    
    @IBOutlet var returningButton: DLRadioButton!
    
    @IBAction func returningButtonAction(_ sender: DLRadioButton) {
        
        behindBaseline = false
        onBaseline = false
        inFrontOfBaseline = false
        returnWinner = false
        unsuccessfulReturn = false
        serverDF = false
        sectionFour = 1
        
        if sender.tag == 1 {
            print("Behind")
            behindBaseline = true
        } else if sender.tag == 2 {
            print("At")
            onBaseline = true
        } else if sender.tag == 3 {
            print("In Front")
            inFrontOfBaseline = true
        } else if sender.tag == 4 {
            print("Winner return")
            returnWinner = true
        } else if sender.tag == 5 {
            print("Unsuccessful Return")
            unsuccessfulReturn = true
        } else if sender.tag == 6 {
            print("Server Double Faulted")
            serverDF = true
        }
    }
    
    @IBOutlet var resultButton: DLRadioButton!
    @IBOutlet var resultTypeButton: DLRadioButton!
    
    @IBAction func resultButtonAction(_ sender: DLRadioButton) {
        
        playerWon = false
        opponentWon = false
        sectionFive = 1
        
        if sender.tag == 1 {
            print("Player Win")
            playerWon = true
        } else if sender.tag == 2 {
            print("Opponent Win")
            opponentWon = true
        }
    }
    
    @IBAction func resultTypeButtonAction(_ sender: DLRadioButton) {
        
        winByAce = false
        winByWinner = false
        winByForced = false
        winByUnforced = false
        sectionSix = 1
        
        if sender.tag == 1 {
            print("Serve Ace Result")
            winByAce = true
        } else if sender.tag == 2 {
            print("Winner")
            winByWinner = true
        } else if sender.tag == 3 {
            print("Forced Error")
            winByForced = true
        } else if sender.tag == 4 {
            print("Unforced Error")
            winByUnforced = true
        }
    }
    
    @IBAction func recordPointButton(_ sender: UIButton) {
        
        var goodToEdit = 1
        
        if (sectionOne == 0 || sectionTwo == 0 || sectionThree == 0 || sectionFour == 0 || sectionFive == 0 || sectionSix == 0) {
            goodToEdit = -1
        }
        
        if (goodToEdit != -1) {
            
            if ((doubleFault && !doubleFaultOne) || (doubleFault && !serverDF)) {
                goodToEdit = 0
            }
            
            if ((doubleFault && serveAce) || (doubleFault && serveWinner) || (doubleFault && serveReturned)) {
                goodToEdit = 0
            }
        
            if ((serveAce && !unsuccessfulReturn) || (serveAce && !winByAce) || (winByAce && !serveAce) || (winByAce && !unsuccessfulReturn)) {
                goodToEdit = 0
            }
            
            if ((serveWinner && !unsuccessfulReturn) || (serveWinner && !winByWinner)) {
                goodToEdit = 0
            }
            
            if ((playerServing && serveAce && opponentWon) || (opponentServing && serveAce && playerWon)) {
                goodToEdit = 0
            }
            
            if ((serveReturned && unsuccessfulReturn)) {
                goodToEdit = 0
            }
            
            if ((playerServing && returnWinner && playerWon) || (opponentServing && returnWinner && opponentWon)) {
                goodToEdit = 0
            }
            
            if (goodToEdit == 1) {
                
                let alert = UIAlertController(title:"Successful", message:"The point has been added successfully.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Ok", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated:true, completion:nil)
                
                numberOfPointsRecorded += 1
                numberOfPoints.text = "Number of points recorded: " + String(numberOfPointsRecorded)
                
                calculateStatistics()
                
                viewWillAppear(true)
                
            } else {
                let alert = UIAlertController(title:"Alert", message:"Incorrect button configuration, please double-check selections and fix.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated:true, completion:nil)
            }
        } else {
            let alert = UIAlertController(title:"Alert", message:"Some buttons not selected, please double-check and select a button in each section.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated:true, completion:nil)
        }
    }
    
    @IBOutlet var numberOfPoints: UILabel!
    
    @IBOutlet var matchTitle: UITextField!
    
    @IBAction func saveMatchButton(_ sender: UIButton) {
        
        if (matchTitle.text == "") {
            let alert = UIAlertController(title:"Alert", message:"No Match Title, please enter a title.", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated:true, completion:nil)
            
        } else {
        
            if (numberOfPointsRecorded != 0) {
            
                calculatePercentages()
                
                let matchStats = PFObject(className:"Match")
                
                matchStats["email"] = PFUser.current()?["email"] as? String
                
                matchStats["title"] = matchTitle.text
                matchStats["numberOfPoints"] = numberOfPointsRecorded
                
                //Player serves
                matchStats["playerTotalFirstServes"] = playerTotalFirstServes
                matchStats["playerTotalSecondServes"] = playerTotalSecondServes
                matchStats["playerFirstServeSuccessful"] = playerFirstServeSuccessful
                matchStats["playerFirstServePercent"] = playerFirstServePercent
                matchStats["playerFirstServePointWins"] = playerFirstServePointWins
                matchStats["playerFirstServePointLosses"] = playerFirstServePointLosses
                matchStats["playerFirstServeAces"] = playerFirstServeAces
                matchStats["playerFirstServeWinners"] = playerFirstServeWinners
                matchStats["playerSecondServePercent"] = playerSecondServePercent
                matchStats["playerSecondServePointWins"] = playerSecondServePointWins
                matchStats["playerSecondServePointLosses"] = playerSecondServePointLosses
                matchStats["playerSecondServeAces"] = playerSecondServeAces
                matchStats["playerSecondServeWinners"] = playerSecondServeWinners
                matchStats["playerDoubleFaults"] = playerDoubleFaults
                
                //Player returns
                matchStats["playerReturnsBehindBaseline"] = playerReturnsBehindBaseline
                matchStats["playerReturnsBehindBaselineWins"] = playerReturnsBehindBaselineWins
                matchStats["playerReturnsBehindBaselineLosses"] = playerReturnsBehindBaselineLosses
                matchStats["playerReturnsOnBaseline"] = playerReturnsOnBaseline
                matchStats["playerReturnsOnBaselineWins"] = playerReturnsOnBaselineWins
                matchStats["playerReturnsOnBaselineLosses"] = playerReturnsOnBaselineLosses
                matchStats["playerReturnsInFrontOfBaseline"] = playerReturnsInFrontOfBaseline
                matchStats["playerReturnsInFrontOfBaselineWins"] = playerReturnsInFrontOfBaselineWins
                matchStats["playerReturnsInFrontOfBaselineLosses"] = playerReturnsInFrontOfBaselineLosses
                matchStats["playerTotalUnsuccessfulReturns"] = playerTotalUnsuccessfulReturns
                
                //Player point results
                matchStats["playerTotalServeAces"] = playerTotalServeAces
                matchStats["playerTotalWinners"] = playerTotalWinners
                matchStats["playerForcedErrors"] = playerForcedErrors
                matchStats["playerUnforcedErrors"] = playerUnforcedErrors
                
                //Opponent serves
                matchStats["opponentTotalFirstServes"] = opponentTotalFirstServes
                matchStats["opponentTotalSecondServes"] = opponentTotalSecondServes
                matchStats["opponentFirstServeSuccessful"] = opponentFirstServeSuccessful
                matchStats["opponentFirstServePercent"] = opponentFirstServePercent
                matchStats["opponentFirstServePointWins"] = opponentFirstServePointWins
                matchStats["opponentFirstServePointLosses"] = opponentFirstServePointLosses
                matchStats["opponentFirstServeAces"] = opponentFirstServeAces
                matchStats["opponentFirstServeWinners"] = opponentFirstServeWinners
                matchStats["opponentSecondServePercent"] = opponentSecondServePercent
                matchStats["opponentSecondServePointWins"] = opponentSecondServePointWins
                matchStats["opponentSecondServePointLosses"] = opponentSecondServePointLosses
                matchStats["opponentSecondServeAces"] = opponentSecondServeAces
                matchStats["opponentSecondServeWinners"] = opponentSecondServeWinners
                matchStats["opponentDoubleFaults"] = opponentDoubleFaults
                
                //Opponenet returns
                matchStats["opponentReturnsBehindBaseline"] = opponentReturnsBehindBaseline
                matchStats["opponentReturnsBehindBaselineWins"] = opponentReturnsBehindBaselineWins
                matchStats["opponentReturnsBehindBaselineLosses"] = opponentReturnsBehindBaselineLosses
                matchStats["opponentReturnsOnBaseline"] = opponentReturnsOnBaseline
                matchStats["opponentReturnsOnBaselineWins"] = opponentReturnsOnBaselineWins
                matchStats["opponentReturnsOnBaselineLosses"] = opponentReturnsOnBaselineLosses
                matchStats["opponentReturnsInFrontOfBaseline"] = opponentReturnsInFrontOfBaseline
                matchStats["opponentReturnsInFrontOfBaselineWins"] = opponentReturnsInFrontOfBaselineWins
                matchStats["opponentReturnsInFrontOfBaselineLosses"] = opponentReturnsInFrontOfBaselineLosses
                matchStats["opponentTotalUnsuccessfulReturns"] = opponentTotalUnsuccessfulReturns
                
                //Opponent point results
                matchStats["opponentTotalServeAces"] = opponentTotalServeAces
                matchStats["opponentTotalWinners"] = opponentTotalWinners
                matchStats["opponentForcedErrors"] = opponentForcedErrors
                matchStats["opponentUnforcedErrors"] = opponentUnforcedErrors
                
                matchStats.saveInBackground {
                    (success: Bool, error: Error?) in
                    if (success) {
                        
                    } else {
                        print("Error saving in background.")
                    }
                }
                
                self.performSegue(withIdentifier: "saveMatch", sender: nil)
                
            } else {
                
                let alert = UIAlertController(title:"Alert", message:"No points recorded, please record at least 1 point.", preferredStyle: UIAlertControllerStyle.alert)
                
                alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
                
                self.present(alert, animated:true, completion:nil)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.matchTitle.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    func calculateStatistics() {
        
        if (playerWon) {
            //Player Won
            
            if (playerServing) {
                //////////////////////////
                //Player serving and won//
                //////////////////////////
                
                if (firstServeIn) {
                    playerTotalFirstServes += 1
                    playerFirstServeSuccessful += 1
                    playerFirstServePointWins += 1
                    
                    
                    if (serveAce) {
                        playerFirstServeAces += 1
                        opponentTotalUnsuccessfulReturns += 1
                        
                    } else if (serveWinner) {
                        playerFirstServeWinners += 1
                        opponentTotalUnsuccessfulReturns += 1
                        
                    } else {
                        //Serve returned
                        
                        if (behindBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsBehindBaseline += 1
                            opponentReturnsBehindBaselineLosses += 1
                            
                        } else if (onBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsOnBaseline += 1
                            opponentReturnsOnBaselineLosses += 1
                            
                        } else if (inFrontOfBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsInFrontOfBaseline += 1
                            opponentReturnsInFrontOfBaselineLosses += 1
                            
                        } else {
                            //UnsuccessfulReturn
                            opponentTotalUnsuccessfulReturns += 1
                            
                        }
                        
                    }
                    
                } else {
                    //Second serve was in
                    playerTotalSecondServes += 1
                    playerTotalFirstServes += 1
                    playerSecondServeSuccessful += 1
                    playerSecondServePointWins += 1
                    
                    if (serveAce) {
                        playerSecondServeAces += 1
                        opponentTotalUnsuccessfulReturns += 1
                        
                    } else if (serveWinner) {
                        playerSecondServeWinners += 1
                        opponentTotalUnsuccessfulReturns += 1
                        
                    } else {
                        //Serve returned
                        if (behindBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsBehindBaseline += 1
                            opponentReturnsBehindBaselineLosses += 1
                            
                        } else if (onBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsOnBaseline += 1
                            opponentReturnsOnBaselineLosses += 1
                            
                        } else if (inFrontOfBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            opponentReturnsInFrontOfBaseline += 1
                            opponentReturnsInFrontOfBaselineLosses += 1
                            
                        } else {
                            //UnsuccessfulReturn
                            opponentTotalUnsuccessfulReturns += 1
                            
                        }
                        
                    }
                }
                
                if (winByAce) {
                    playerTotalServeAces += 1
                } else if (winByWinner) {
                    playerTotalWinners += 1
                    opponentForcedErrors += 1
                } else if (winByForced) {
                    opponentForcedErrors += 1
                } else if (winByUnforced){
                    opponentUnforcedErrors += 1
                }
            } else {
                ///////////////////////////////////
                //Opponent serving and player won//
                ///////////////////////////////////
                
                if (firstServeIn) {
                    opponentTotalFirstServes += 1
                    opponentFirstServeSuccessful += 1
                    opponentFirstServePointLosses += 1
                    
                    if (behindBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsBehindBaseline += 1
                        playerReturnsBehindBaselineWins += 1
                        
                    } else if (onBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsOnBaseline += 1
                        playerReturnsOnBaselineWins += 1
                        
                    } else if (inFrontOfBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsInFrontOfBaseline += 1
                        playerReturnsInFrontOfBaselineWins += 1
                        
                    } else if (returnWinner) {
                        playerTotalSuccessfulReturns += 1
                        
                    }
                    
                    
                } else if (secondServeIn) {
                    opponentTotalSecondServes += 1
                    opponentTotalFirstServes += 1
                    opponentSecondServeSuccessful += 1
                    opponentSecondServePointLosses += 1
                    
                    if (behindBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsBehindBaseline += 1
                        playerReturnsBehindBaselineWins += 1
                        
                    } else if (onBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsOnBaseline += 1
                        playerReturnsOnBaselineWins += 1
                        
                    } else if (inFrontOfBaseline) {
                        playerTotalSuccessfulReturns += 1
                        playerReturnsInFrontOfBaseline += 1
                        playerReturnsInFrontOfBaselineWins += 1
                        
                    } else if (returnWinner) {
                        playerTotalSuccessfulReturns += 1
                        
                    }
                    
                } else {
                    //double fault
                    opponentTotalFirstServes += 1
                    opponentTotalSecondServes += 1
                    opponentDoubleFaults += 1
                }
                
                if (winByWinner) {
                    playerTotalWinners += 1
                    opponentForcedErrors += 1
                } else if (winByForced) {
                    opponentForcedErrors += 1
                } else if (winByUnforced){
                    opponentUnforcedErrors += 1
                }
            }
            
        } else {
            //Opponent Won
            
            if (playerServing) {
                ///////////////////////////
                //Player serving and lost//
                ///////////////////////////
                
                if (firstServeIn) {
                    playerTotalFirstServes += 1
                    playerFirstServeSuccessful += 1
                    playerFirstServePointLosses += 1
                    
                    if (behindBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsBehindBaseline += 1
                        opponentReturnsBehindBaselineWins += 1
                        
                    } else if (onBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsOnBaseline += 1
                        opponentReturnsOnBaselineWins += 1
                        
                    } else if (inFrontOfBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsInFrontOfBaseline += 1
                        opponentReturnsInFrontOfBaselineWins += 1
                        
                    } else if (returnWinner) {
                        opponentTotalSuccessfulReturns += 1
                        
                    }
                    
                } else if (secondServeIn) {
                    playerTotalSecondServes += 1
                    playerTotalFirstServes += 1
                    playerSecondServeSuccessful += 1
                    playerSecondServePointLosses += 1
                    
                    if (behindBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsBehindBaseline += 1
                        opponentReturnsBehindBaselineWins += 1
                        
                    } else if (onBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsOnBaseline += 1
                        opponentReturnsOnBaselineWins += 1
                        
                    } else if (inFrontOfBaseline) {
                        opponentTotalSuccessfulReturns += 1
                        opponentReturnsInFrontOfBaseline += 1
                        opponentReturnsInFrontOfBaselineWins += 1
                        
                    } else if (returnWinner) {
                        opponentTotalSuccessfulReturns += 1
                        
                    }
                    
                } else {
                    //double fault
                    playerTotalFirstServes += 1
                    playerTotalSecondServes += 1
                    playerDoubleFaults += 1
                }
                
                if (winByWinner) {
                    opponentTotalWinners += 1
                    playerForcedErrors += 1
                } else if (winByForced) {
                    playerForcedErrors += 1
                } else if (winByUnforced){
                    playerUnforcedErrors += 1
                }
                
            } else {
                ////////////////////////////////////
                //Opponent serving and player lost//
                ////////////////////////////////////
                
                if (firstServeIn) {
                    opponentTotalFirstServes += 1
                    opponentFirstServeSuccessful += 1
                    opponentFirstServePointWins += 1
                    
                    if (serveAce) {
                        opponentFirstServeAces += 1
                        opponentTotalServeAces += 1
                        playerTotalUnsuccessfulReturns += 1
                        
                    } else if (serveWinner) {
                        opponentFirstServeWinners += 1
                        playerTotalUnsuccessfulReturns += 1
                        
                    } else {
                        //Serve returned
                        
                        if (behindBaseline) {
                            playerTotalSuccessfulReturns += 1
                            playerReturnsBehindBaseline += 1
                            playerReturnsBehindBaselineLosses += 1
                            
                        } else if (onBaseline) {
                            playerTotalSuccessfulReturns += 1
                            playerReturnsOnBaseline += 1
                            playerReturnsOnBaselineLosses += 1
                            
                        } else if (inFrontOfBaseline) {
                            playerTotalSuccessfulReturns += 1
                            playerReturnsInFrontOfBaseline += 1
                            playerReturnsInFrontOfBaselineLosses += 1
                            
                        } else {
                            //UnsuccessfulReturn
                            playerTotalUnsuccessfulReturns += 1
                            
                        }
                        
                    }
                    
                } else {
                    //Second serve was in
                    opponentTotalSecondServes += 1
                    opponentTotalFirstServes += 1
                    opponentSecondServeSuccessful += 1
                    opponentSecondServePointWins += 1
                    
                    if (serveAce) {
                        opponentSecondServeAces += 1
                        opponentTotalServeAces += 1
                        playerTotalUnsuccessfulReturns += 1
                        
                    } else if (serveWinner) {
                        opponentSecondServeWinners += 1
                        playerTotalUnsuccessfulReturns += 1
                        
                    } else {
                        //Serve returned
                        if (behindBaseline) {
                            playerTotalSuccessfulReturns += 1
                            playerReturnsBehindBaseline += 1
                            playerReturnsBehindBaselineLosses += 1
                            
                        } else if (onBaseline) {
                            opponentTotalSuccessfulReturns += 1
                            playerReturnsOnBaseline += 1
                            playerReturnsOnBaselineLosses += 1
                            
                        } else if (inFrontOfBaseline) {
                            playerTotalSuccessfulReturns += 1
                            playerReturnsInFrontOfBaseline += 1
                            playerReturnsInFrontOfBaselineLosses += 1
                            
                        } else {
                            //UnsuccessfulReturn
                            playerTotalUnsuccessfulReturns += 1
                            
                        }
                    }
                }
                
                if (winByWinner) {
                    opponentTotalWinners += 1
                    playerForcedErrors += 1
                } else if (winByForced) {
                    playerForcedErrors += 1
                } else if (winByUnforced){
                    playerUnforcedErrors += 1
                }
            }
        }
    }
    
    func calculatePercentages() {
        
        if (playerTotalFirstServes != 0) {
            playerFirstServePercent = (round((playerFirstServeSuccessful/playerTotalFirstServes)*100)*100)/100
        } else {
            playerFirstServePercent = -1
        }
        
        if (playerFirstServeSuccessful != 0) {
            playerFirstServePointWins = (round((playerFirstServePointWins/playerFirstServeSuccessful)*100)*100)/100
        } else {
            playerFirstServePointWins = 0
        }
        
        if (playerSecondServeSuccessful != 0) {
            playerSecondServePointWins = (round((playerSecondServePointWins/playerSecondServeSuccessful)*100)*100)/100
        } else {
            playerSecondServePointWins = 0
        }
        
        if (playerTotalSecondServes != 0) {
            playerSecondServePercent = (round((playerSecondServeSuccessful/playerTotalSecondServes)*100)*100)/100
        } else {
            playerSecondServePercent = -1
        }
            
        if (opponentTotalFirstServes != 0) {
            opponentFirstServePercent = (round((opponentFirstServeSuccessful/opponentTotalFirstServes)*100)*100)/100
        } else {
           opponentFirstServePercent = -1
        }
        
        if (opponentFirstServeSuccessful != 0) {
            opponentFirstServePointWins = (round((opponentFirstServePointWins/opponentFirstServeSuccessful)*100)*100)/100
        } else {
           opponentFirstServePointWins = 0
        }
        
        if (opponentSecondServeSuccessful != 0) {
            opponentSecondServePointWins = (round((opponentSecondServePointWins/opponentSecondServeSuccessful)*100)*100)/100
        } else {
           opponentSecondServePointWins = 0
        }
                
        if (opponentTotalSecondServes != 0) {
            opponentSecondServePercent = (round((opponentSecondServeSuccessful/opponentTotalSecondServes)*100)*100)/100
        } else {
           opponentSecondServePercent = -1
        }
        
        if (playerTotalSuccessfulReturns != 0) {
            playerReturnsBehindBaseline = (round((playerReturnsBehindBaseline/playerTotalSuccessfulReturns)*100)*100)/100
            playerReturnsOnBaseline = (round((playerReturnsOnBaseline/playerTotalSuccessfulReturns)*100)*100)/100
            playerReturnsInFrontOfBaseline = (round((playerReturnsInFrontOfBaseline/playerTotalSuccessfulReturns)*100)*100)/100
        } else {
           playerReturnsBehindBaseline = -1
           playerReturnsOnBaseline = -1
           playerReturnsInFrontOfBaseline = -1
        }
        
        if ((playerReturnsBehindBaselineWins+playerReturnsBehindBaselineLosses) != 0) {
            let wins : Double = playerReturnsBehindBaselineWins
            
            playerReturnsBehindBaselineWins = (round((playerReturnsBehindBaselineWins/(playerReturnsBehindBaselineWins+playerReturnsBehindBaselineLosses))*100)*100)/100
            
            playerReturnsBehindBaselineLosses = (round((playerReturnsBehindBaselineLosses/(wins+playerReturnsBehindBaselineLosses))*100)*100)/100
        } else {
           playerReturnsBehindBaselineWins = -1
           playerReturnsBehindBaselineLosses = -1
        }
        
        if ((playerReturnsOnBaselineWins+playerReturnsOnBaselineLosses) != 0) {
            let wins : Double = playerReturnsOnBaselineWins
            playerReturnsOnBaselineWins = (round((playerReturnsOnBaselineWins/(playerReturnsOnBaselineWins+playerReturnsOnBaselineLosses))*100)*100)/100
            
            playerReturnsOnBaselineLosses = (round((playerReturnsOnBaselineLosses/(wins+playerReturnsOnBaselineLosses))*100)*100)/100
        } else {
           playerReturnsOnBaselineWins = -1
           playerReturnsOnBaselineLosses = -1
        }
        
        if ((playerReturnsInFrontOfBaselineWins+playerReturnsInFrontOfBaselineLosses) != 0) {
            let wins : Double = playerReturnsInFrontOfBaselineWins
            playerReturnsInFrontOfBaselineWins = (round((playerReturnsInFrontOfBaselineWins/(playerReturnsInFrontOfBaselineWins+playerReturnsInFrontOfBaselineLosses))*100)*100)/100
            
            playerReturnsInFrontOfBaselineLosses = (round((playerReturnsInFrontOfBaselineLosses/(wins+playerReturnsInFrontOfBaselineLosses))*100)*100)/100
        } else {
           playerReturnsInFrontOfBaselineWins = -1
           playerReturnsInFrontOfBaselineLosses = -1
        }
        
        if (opponentTotalSuccessfulReturns != 0) {
            opponentReturnsBehindBaseline = (round((opponentReturnsBehindBaseline/opponentTotalSuccessfulReturns)*100)*100)/100
            opponentReturnsOnBaseline = (round((opponentReturnsOnBaseline/opponentTotalSuccessfulReturns)*100)*100)/100
            opponentReturnsInFrontOfBaseline = (round((opponentReturnsInFrontOfBaseline/opponentTotalSuccessfulReturns)*100)*100)/100
        } else {
           opponentReturnsBehindBaseline = -1
           opponentReturnsOnBaseline = -1
           opponentReturnsInFrontOfBaseline = -1
        }
        
        if ((opponentReturnsBehindBaselineWins+opponentReturnsBehindBaselineLosses) != 0) {
            let wins : Double = opponentReturnsBehindBaselineWins
            opponentReturnsBehindBaselineWins = (round((opponentReturnsBehindBaselineWins/(opponentReturnsBehindBaselineWins+opponentReturnsBehindBaselineLosses))*100)*100)/100
            
            opponentReturnsBehindBaselineLosses = (round((opponentReturnsBehindBaselineLosses/(wins+opponentReturnsBehindBaselineLosses))*100)*100)/100
        } else {
           opponentReturnsBehindBaselineWins = -1
           opponentReturnsBehindBaselineLosses = -1
        }
        
        if ((opponentReturnsOnBaselineWins+opponentReturnsOnBaselineLosses) != 0) {
            let wins : Double = opponentReturnsOnBaselineWins
            opponentReturnsOnBaselineWins = (round((opponentReturnsOnBaselineWins/(opponentReturnsOnBaselineWins+opponentReturnsOnBaselineLosses))*100)*100)/100
            
            opponentReturnsOnBaselineLosses = (round((opponentReturnsOnBaselineLosses/(wins+opponentReturnsOnBaselineLosses))*100)*100)/100
        } else {
           opponentReturnsOnBaselineWins = -1
           opponentReturnsOnBaselineLosses = -1
        }
        
        if ((opponentReturnsInFrontOfBaselineWins+opponentReturnsInFrontOfBaselineLosses) != 0) {
            let wins : Double = opponentReturnsInFrontOfBaselineWins
            opponentReturnsInFrontOfBaselineWins = (round((opponentReturnsInFrontOfBaselineWins/(opponentReturnsInFrontOfBaselineWins+opponentReturnsInFrontOfBaselineLosses))*100)*100)/100
            
            opponentReturnsInFrontOfBaselineLosses = (round((opponentReturnsInFrontOfBaselineLosses/(wins+opponentReturnsInFrontOfBaselineLosses))*100)*100)/100
        } else {
           opponentReturnsInFrontOfBaselineWins = -1
           opponentReturnsInFrontOfBaselineLosses = -1
        }
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
