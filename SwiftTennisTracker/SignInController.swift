//
//  SignInController.swift
//  SwiftTennisTracker
//
//  Created by Bryan Winmill on 11/3/17.
//  Copyright © 2017 Bryan Winmill. All rights reserved.
//

import Foundation
import UIKit
import Parse

class SignInController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var email: UITextField!
    @IBOutlet var password: UITextField!
    
    @IBAction func signInButton(_ sender: UIButton) {
        
        if (email.text == "" || password.text == "") {
            
            var alert = UIAlertController(title:"Alert", message:"Some fields are blank!", preferredStyle: UIAlertControllerStyle.alert)
            
            alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
            
            self.present(alert, animated:true, completion:nil)
            
        } else {
        
            [PFUser.logInWithUsername(inBackground: email.text!, password: password.text!, block: { (user, error) in
                if(user != nil) {
                    
                    self.performSegue(withIdentifier: "signIn", sender: nil)
                }
                else {
                    
                    var alert = UIAlertController(title:"Alert", message:"Invalid email and password!", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alert.addAction(UIAlertAction(title:"Close", style: UIAlertActionStyle.default, handler: nil))
                    
                    self.present(alert, animated:true, completion:nil)
                }
            })]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.email.delegate = self
        self.password.delegate = self
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.view.endEditing(true)
        
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
